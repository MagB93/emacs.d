(require 'package)

(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
;; (add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))
(add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages/"))

(setq package-enable-at-startup nil)

;; if not yet installed, install package use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(eval-when-compile (require 'use-package))

;; Recursive function from https://www.emacswiki.org/emacs/DotEmacsModular
(defun load-directory (directory)
  "Load recursively all `.el' files in DIRECTORY."
  (dolist (element (directory-files-and-attributes directory nil nil nil))
    (let* ((path (car element))
           (fullpath (concat directory "/" path))
           (isdir (car (cdr element)))
           (ignore-dir (or (string= path ".") (string= path ".."))))
      (cond
       ((and (eq isdir t) (not ignore-dir))
        (load-directory fullpath))
       ((and (eq isdir nil) (string= (substring path -3) ".el"))
        (load (file-name-sans-extension fullpath)))))))

(load-directory "~/.emacs.d/lisp")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(haskell-font-lock-symbols t)
 '(haskell-font-lock-symbols-alist
   (and
    (fboundp 'decode-char)
    (list
     (cons "&&"
           (decode-char 'ucs 57600))
     (cons "***"
           (decode-char 'ucs 57601))
     (cons "*>"
           (decode-char 'ucs 57602))
     (cons "\\\\"
           (decode-char 'ucs 57603))
     (cons "||"
           (decode-char 'ucs 57604))
     (cons "|>"
           (decode-char 'ucs 57605))
     (cons "::"
           (decode-char 'ucs 57606))
     (cons "=="
           (decode-char 'ucs 57607))
     (cons "==="
           (decode-char 'ucs 57608))
     (cons "==>"
           (decode-char 'ucs 57609))
     (cons "=>"
           (decode-char 'ucs 57610))
     (cons "=<<"
           (decode-char 'ucs 57611))
     (cons "!!"
           (decode-char 'ucs 57612))
     (cons ">>"
           (decode-char 'ucs 57613))
     (cons ">>="
           (decode-char 'ucs 57614))
     (cons ">>>"
           (decode-char 'ucs 57615))
     (cons ">>-"
           (decode-char 'ucs 57616))
     (cons ">-"
           (decode-char 'ucs 57617))
     (cons "->"
           (decode-char 'ucs 57618))
     (cons "-<"
           (decode-char 'ucs 57619))
     (cons "-<<"
           (decode-char 'ucs 57620))
     (cons "<*"
           (decode-char 'ucs 57621))
     (cons "<*>"
           (decode-char 'ucs 57622))
     (cons "<|"
           (decode-char 'ucs 57623))
     (cons "<|>"
           (decode-char 'ucs 57624))
     (cons "<$>"
           (decode-char 'ucs 57625))
     (cons "<>"
           (decode-char 'ucs 57626))
     (cons "<-"
           (decode-char 'ucs 57627))
     (cons "<<"
           (decode-char 'ucs 57628))
     (cons "<<<"
           (decode-char 'ucs 57629))
     (cons "<+>"
           (decode-char 'ucs 57630))
     (cons ".."
           (decode-char 'ucs 57631))
     (cons "..."
           (decode-char 'ucs 57632))
     (cons "++"
           (decode-char 'ucs 57633))
     (cons "+++"
           (decode-char 'ucs 57634))
     (cons "/="
           (decode-char 'ucs 57635)))))
 '(inhibit-startup-screen t)
 '(package-selected-packages
   '(helm-projectile yasnippet-snippets yaml-mode which-key use-package toml-mode rainbow-delimiters rainbow-blocks paredit org-bullets mmm-mode lsp-ui lsp-python lsp-fortran ivy-xref intero hindent highlight-indentation helm ghci-completion general function-args flx fill-column-indicator eyebrowse evil-surround evil-smartparens evil-org evil-magit evil-commentary ess doom-themes doom-modeline diff-hl counsel-projectile company-lsp company-box cmake-font-lock clang-format ccls avy auctex ac-slime)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
