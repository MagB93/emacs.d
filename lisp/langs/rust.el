(use-package rust-mode
    :ensure t
    :mode "\\.rs\\'"
    :init
    (setq rust-format-on-save t))

(use-package cargo
  :ensure t
  :config

  (setq cargo-process--command-fmt "+nightly fmt")
  (add-hook 'rust-mode-hook 'cargo-minor-mode))

(use-package lsp-rust
  :after lsp-mode
  :config
  (setq lsp-rust-rls-command '("rustup" "run" "nightly" "rls"))
  (add-hook 'rust-mode-hook #'lsp-rust-enable)
  (add-hook 'rust-mode-hook #'flycheck-mode))
