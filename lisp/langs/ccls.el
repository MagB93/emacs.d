(defun ccls//enable ()
  (condition-case nil
      (lsp-ccls-enable)
    (user-error nil)))

(use-package ccls
  :commands lsp-ccls-enable
  :init
  (add-hook 'c-mode-hook #'ccls//enable)
  (add-hook 'c++-mode-hook #'ccls//enable)
  :config
  (add-hook 'xref-backend-functions 'lsp--xref-backend)
  (add-hook 'completion-at-point-functions' lsp-completion-at-point)

  (setq ccls-executable "/home/mbad/opt/ccls/build/ccls")

  ;; Log file
  (setq ccls-extra-args '("--log-file=/tmp/ccls.log"))

  ;; Cache directory, both relative and absolute paths are supported
  (setq ccls-cache-dir "~/.cache/ccls")

  ;; Better ui
  (define-key evil-normal-state-map (kbd "C-p") 'lsp-ui-peek-jump-forward)
  (define-key evil-normal-state-map (kbd "C-t") 'lsp-ui-peek-jump-backward)

  ;; Company completion
  (setq company-transformers nil company-lsp-async t company-lsp-cache-candidates nil)

  ;; Semantic highlighting
  (setq ccls-sem-highlight-method 'font-lock)

  ;; For rainbow semantic highlighting
  (ccls-use-default-rainbow-sem-highlight)
  (ccls-call-hierarchy nil) ; caller hierarchy
  (ccls-call-hierarchy t) ; callee hierarchy
  (ccls-inheritance-hierarchy nil) ; base hierarchy
  (ccls-inheritance-hierarchy t) ; derived hierarchy
  )
