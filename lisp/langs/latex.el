(use-package flymake
    :defer t
    :ensure t
    :config
    (defun flymake-get-tex-args (file-name)
    (list "pdflatex"
    (list "-file-line-error" "-draftmode" "-interaction=nonstopmode" file-name))
    (add-hook 'LaTeX-mode-hook 'flymake-mode))
    (setq ispell-program-name "aspell") ; could be ispell as well, depending on your preferences
    (setq ispell-dictionary "english") ; this can obviously be set to any language your spell-checking program supports
)

(use-package tex-mode
  :ensure auctex
  :config
  (setq TeX-auto-save t
      TeX-parse-self t)
  (setq-default TeX-master nil)
      (add-hook 'LaTeX-mode-hook 'flyspell-mode)
      (add-hook 'LaTeX-mode-hook 'flyspell-buffer)
      (setq TeX-save-quere nil)
      (add-hook 'LaTeX-mode-hook 'turn-on-auto-fill)

  (add-hook 'LaTeX-mode-hook
          (lambda ()
              (rainbow-delimiters-mode)
              (company-mode)
              (smartparens-mode)
              (turn-on-reftex)
              (setq reftex-plug-into-AUCTeX t)
              (reftex-isearch-minor-mode)
              (setq TeX-PDF-mode t)
              (setq TeX-source-correlate-method 'synctex)
              (setq TeX-source-correlate-start-server t)))

  ;; Now we want to make the folding of sections possible
  (defun turn-on-outline-minor-mode ()
  (outline-minor-mode 1))
  (add-hook 'LaTeX-mode-hook 'turn-on-outline-minor-mode)
  (setq outline-minor-mode-prefix "\C-c \C-o") ; Or something else
  ;; Hide all contents of the current section: C-c C-o C-l
  ;; Move to the next unit of the document C-c C-o C-n
  ;; Move to the previous unit of the document C-c C-o C-p
  ;; See the entire document again C-c C-o C-a

  ;; Update PDF buffers after successful LaTeX runs
  (add-hook 'TeX-after-TeX-LaTeX-command-finished-hook
          #'TeX-revert-document-buffer)

  ;; to use pdfview with auctex
  (add-hook 'LaTeX-mode-hook 'pdf-tools-install)

  ;; to use pdfview with auctex
  (setq TeX-view-program-selection '((output-pdf "pdf-tools"))
      TeX-source-correlate-start-server t)
  (setq TeX-view-program-list '(("pdf-tools" "TeX-pdf-tools-sync-view")))
)

(use-package reftex
  :after latex
  :ensure t
  :config
    (autoload 'reftex-mode "reftex" "RefTeX Minor Mode" t)
    (autoload 'turn-on-reftex "reftex" "RefTeX Minor Mode" nil)
    (autoload 'reftex-citation "reftex-cite" "Make citation" nil)
    (autoload 'reftex-index-phrase-mode "reftex-index" "Phrase Mode" t)
    (add-hook 'latex-mode-hook 'turn-on-reftex) ; with Emacs latex mode
    (add-hook 'reftex-load-hook 'imenu-add-menubar-index)
    (add-hook 'LaTeX-mode-hook 'turn-on-reftex)
    (setq reftex-plug-into-AUCtex t)
    (setq LaTeX-eqnarray-label "eq"
      LaTeX-equation-label "eq"
      LaTeX-figure-label "fig"
      LaTeX-table-label "tab"
      LaTeX-myChapter-label "chap"
      TeX-auto-save t
      TeX-newline-function 'reindent-then-newline-and-indent
      TeX-parse-self t
      LaTeX-section-hook
      '(LaTeX-section-heading
        LaTeX-section-title
        LaTeX-section-toc
        LaTeX-section-section
        LaTeX-section-label)
    )
)
