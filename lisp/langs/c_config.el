(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))


(setq c-default-style "bsd"
      c-basic-offset 2
      )

(use-package clang-format
  :ensure t
  :config
  (global-set-key [C-M-tab] 'clang-format-region)
)

(use-package function-args
  :ensure t
  :config
  (fa-config-default))
