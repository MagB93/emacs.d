;; (use-package lsp-mode
;;     :ensure t
;;     :defer t
;;     :config
;;     (add-hook 'lsp-after-open-hook 'lsp-enable-imenu)

;;     ;; Other config for lsp
;;     (setq lsp-highlight-symbol t)
;; )
(add-to-list 'load-path "/home/mbad/opt/lsp-mode")
(require 'lsp-mode)

(use-package lsp-ui
  :ensure t
  :defer t
  :config
  (add-hook 'lsp-mode-hook 'lsp-ui-mode)
  (setq
   lsp-ui-sideline-ignore-duplicate t
   lsp-ui-imenu t
   lsp-ui-imenu-kind-position 'left
   lsp-ui-imenu-mode-map 't
   (define-key lsp-ui-mode-map (kbd "lA") 'lsp-ui-peek-find-workspace-symbol)
   (define-key lsp-ui-mode-map (kbd "lF") 'lsp-format-buffer)
   (define-key lsp-ui-mode-map (kbd "ll") 'lsp-ui-sideline-mode)
   (define-key lsp-ui-mode-map (kbd "ld") 'lsp-ui-doc-mode)
   (define-key lsp-ui-mode-map (kbd "lr") 'lsp-rename)
   (define-key lsp-ui-mode-map (kbd "lf") 'ccls-freshen-index)

   (define-key lsp-ui-peek (kbd "h") 'lsp-ui-peek--select-prev-file)
   (define-key lsp-ui-peek (kbd "j") 'lsp-ui-peek--select-next)
   (define-key lsp-ui-peek (kbd "k") 'lsp-ui-peek--select-prev)
   (define-key lsp-ui-peek (kbd "l") 'lsp-ui-peek--select-next-file)
   )

  )

(use-package company-lsp
  :ensure t
  :after company lsp-mode
  :init
  (push 'company-lsp company-backends)
  )
