(setq evil-want-C-u-scroll 't)
(use-package evil
  :ensure t
  :init
  (setq evil-shift-width 2)
  :config
  (evil-mode 1)
)

(use-package evil-commentary
  :ensure t
  :config
  (evil-commentary-mode)
)

(use-package evil-magit
  :ensure t
)

(use-package evil-smartparens
  :ensure t
  :config
  (add-hook 'smart-parens-enabled-hook #'evil-smartparens-mode)
)

(use-package evil-surround
 :ensure t
 :config
 (global-evil-surround-mode 1)
 )

(use-package vimish-fold
  :ensure t
  :config
  (global-set-key (kbd "<menu> v f") #'vimish-fold)
  (global-set-key (kbd "<menu> v v") #'vimish-fold-delete)
  (vimish-fold-global-mode 1)
)
