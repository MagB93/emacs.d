
  (setq-default indent-tabs-mode nil)
  (setq-default tab-width 2)

  ;; Make tab key call the indent command
  (setq-default tab-always-indent t)

  ;; Make the tab key indent first then completion
  (setq-default tab-always-indent 'complete)
  (setq-default show-trailing-whitespace t)

  ;; Remove trailing-white space after save
  (add-hook 'before-save-hook 'delete-trailing-whitespace)

  (setq display-line-numbers 'relative)
  (setq display-line-numbers-type 'relative)
  (setq display-line-numbers-current-absolute t)

  (global-display-line-numbers-mode t)
