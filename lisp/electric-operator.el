(use-package electric-operator
  :ensure t
  :config
  (add-hook 'c-mode-hook #'electric-operator-mode)
  (add-hook 'c++-mode-hook #'electric-operator-mode)
  (add-hook 'fortran-mode-hook #'electric-operator-mode)
  (add-hook 'pypthon-mode-hook #'electric-operator-mode)
  (add-hook 'julia-mode-hook #'electric-operator-mode)
  (add-hook 'js-mode-hook #'electric-operator-mode)
)
