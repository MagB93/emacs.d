(use-package rainbow-delimiters
  :ensure t
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
  (add-hook 'org-mode-hook #'rainbow-delimiters-mode)
)

(use-package rainbow-blocks
  :ensure t
  :config
  (add-hook 'prog-mode-hook #'rainbow-blocks-mode)
)

(setq-default fill-column 120)
(use-package fill-column-indicator
  :ensure t
  :config
  (setq fci-rule-width 1)
  (add-hook 'prog-mode-hook 'fci-mode)
)

(use-package highlight-indentation
  :ensure t
  :config
  (set-face-background 'highlight-indentation-face "#e3e3d3")
  (set-face-background 'highlight-indentation-current-column-face "#c3b3b3")
  (highlight-indentation-mode t)
  (highlight-indentation-current-column-mode t)
  (add-hook 'prog-mode-hook #'highlight-indentation-mode)
  (add-hook 'prog-mode-hook #'highlight-indentation-current-column-mode)
  )


(use-package smartparens
  :ensure t
  :config
  (add-hook 'prog-mode-hook 'smartparens-mode)
  (add-hook 'org-mode-hook 'smartparens-mode)
)

(use-package auto-highlight-symbol
  :init
  (global-auto-highlight-symbol-mode 1)
)
