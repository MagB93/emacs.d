
(use-package projectile
  :ensure t
  :demand t
  :diminish projectile-mode
  :init
    (use-package helm-projectile
  :ensure t
  :config
    (progn
      (setq projectile-completion-system 'helm)
      (helm-projectile-on)
      )
    )
  :config
    (projectile-global-mode)
  :bind-keymap
  (("C-c p" . projectile-command-map)
   ("C-c <f6>" . projectile-find-other-file))
  )

  (use-package magit
    :ensure t
    :diminish auto-revert-mode
    :commands (magit-status magit-checkout)
    :bind (("C-x g" . magit-status))
    :init
    (setq magit-revert-buffers 'silent
          magit-push-always-verify nil
          git-commit-summary-max-length 70)
  )

  (use-package diff-hl
    :ensure t
    :defer t
    :init
    (add-hook 'prog-mode-hook #'diff-hl-mode 'append)
    (add-hook 'org-mode-hook #'diff-hl-mode 'append)
    (add-hook 'magit-post-refresh-hook #'diff-hl-magit-post-refresh)
  )
