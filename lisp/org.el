(require 'org)

;; 'Copied' from doom-emacs,
;; source https://github.com/hlissner/doom-emacs/blob/master/modules/lang/org/autoload/org.el#L169
(defun +org/insert-item (direction)
     "Inserts a new heading, table cell or item, depending on the context.
DIRECTION can be 'above or 'below.
I use this instead of `org-insert-item' or `org-insert-heading' which are too
opinionated and perform this simple task incorrectly (e.g. whitespace in the
wrong places)."
     (interactive)
     (let* ((context (org-element-lineage
                      (org-element-context)
                      '(table table-row headline inlinetask item plain-list)
                      t))
            (type (org-element-type context)))
       (cond ((memq type '(item plain-list))
              (let ((marker (org-element-property :bullet context))
                    (pad (save-excursion
                           (back-to-indentation)
                           (- (point) (line-beginning-position)))))
                (pcase direction
                  ('below
                   (org-end-of-item)
                   (goto-char (line-beginning-position))
                   (insert (make-string pad 32) (or marker ""))
                   (save-excursion (insert "\n")))
                  ('above
                   (goto-char (line-beginning-position))
                   (insert (make-string pad 32) (or marker ""))
                   (save-excursion (insert "\n")))))
              (when (org-element-property :checkbox context)
                (insert "[ ] ")))

             ((memq type '(table table-row))
              (pcase direction
                ('below (org-table-insert-row t))
                ('above (org-shiftmetadown))))

             ((memq type '(headline inlinetask))
              (let ((level (if (eq (org-element-type context) 'headline)
                               (org-element-property :level context)
                             1)))
                (pcase direction
                  ('below
                   (let ((at-eol (= (point) (1- (line-end-position)))))
                     (goto-char (line-end-position))
                     (org-end-of-subtree)
                     (insert (concat "\n"
                                     (when (= level 1)
                                       (if at-eol
                                           (ignore (cl-incf level))
                                         "\n"))
                                     (make-string level ?*)
                                     " "))))
                  ('above
                   (org-back-to-heading)
                   (org-insert-heading)
                   (when (= level 1)
                     (save-excursion (insert "\n")))))
                (when (org-element-property :todo-type context)
                  (org-todo 'todo))))

             (t (user-error "Not a valid list, heading or table")))

       (when (bound-and-true-p evil-mode)
         (evil-append-line 1))))

(global-set-key (kbd "M-RET") '(+org/insert-item 'below))

(use-package org-bullets
    :ensure t
    :config
    (add-hook 'org-mode-hook (lambda() (org-bullets-mode 1)))
)

(use-package evil-org
  :ensure t
  :config
  (add-hook 'org-mode-hook 'evil-org-mode)
)
