(use-package lispy
  :ensure t
  )

(use-package paredit
  :ensure t
  :config
  (autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
  (add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
  (add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
  (add-hook 'ielm-mode-hook             #'enable-paredit-mode)
  (add-hook 'lisp-mode-hook             #'enable-paredit-mode)
  (add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
  (add-hook 'scheme-mode-hook           #'enable-paredit-mode)
  )

(use-package eldoc
  :ensure t
  :config
  (add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)
  (add-hook 'lisp-interaction-mode-hook 'turn-on-eldoc-mode)
  (add-hook 'ielm-mode-hook 'turn-on-eldoc-mode)
  (eldoc-add-command
     'paredit-backward-delete
     'paredit-close-round)
)

  (use-package ac-slime
    :ensure t
    :config
    (add-hook 'slime-mode-hook 'set-up-slime-ac)
    (add-hook 'slime-repl-mode-hook 'set-up-slime-ac)
    (eval-after-load "auto-complete" '(add-to-list 'ac-modes 'slime-repl-mode 'emacs-lisp-mode))

    (defun ielm-auto-complete ()
    "Enables `auto-complete' support in \\[ielm]."
    (setq ac-sources '(ac-source-functions
                       ac-source-variables
                       ac-source-features
                       ac-source-symbols
                       ac-source-words-in-same-mode-buffers))
    (add-to-list 'ac-modes 'inferior-emacs-lisp-mode)
    (auto-complete-mode 1))
    (add-hook 'ielm-mode-hook 'ielm-auto-complete)

    (add-hook 'ielm-mode-hook #'enable-paredit-mode)
    (add-hook 'ielm-mode-hook (lambda () (set (make-local-variable 'company-backends) '(company-elisp))))

    (add-hook 'emacs-lisp-mode-hook #'enable-paredit-mode)
    (add-hook 'emacs-lisp-mode-hook (lambda () (set (make-local-variable 'company-backends) '(company-elisp))))
    (add-hook 'emacs-lisp-mode-hook 'ielm-auto-complete)
  )
