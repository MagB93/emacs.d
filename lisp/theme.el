(use-package color-theme-modern
  :ensure t
  :config

  ;; (load-theme 'aalto-light t)
  (load-theme 'aliceblue t)
  )

(add-hook 'prog-mode-hook 'column-number-mode)
